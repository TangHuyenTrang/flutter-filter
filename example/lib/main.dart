import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:photofilters/photofilters.dart';
import 'package:image/image.dart' as imageLib;
import 'package:image_picker/image_picker.dart';


import 'a.dart' as a;
import 'package:photofilters/painter.dart';


void main() => runApp(new MaterialApp(home: MyApp()));

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _finished = true;
  PainterController _controller;

  @override
  void initState() {
    super.initState();
    _finished = false;
    _controller = _newController();
  }
  String fileName;
  //List<Filter> filters = presetCustomerFiltersList;
  //applyFilter(255, 50, 80, 0.12);
  List<Filter> filters = a.applyFilter(225, 240, 0, 0.1,0.3);
  String rawJson1 = '{"a":[240, 248, 255, 1], "antiquewhite" : [250, 235, 215, 1]}';

  //List<Filter> filters = a.applyFilter(0, 255, 255, 0.12,0.05 );
  // Map<String, dynamic> user = jsonDecode(rawJson1);
  //
  // Map userMap = jsonDecode(rawJson1);
  // var colors = Color.fromJson(userMap);


  File imageFile;

  Future getImage(context) async {
    imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    fileName = basename(imageFile.path);
    var image = imageLib.decodeImage(imageFile.readAsBytesSync());
    image = imageLib.copyResize(image, width: 100);
    Map imagefile = await Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => new PhotoFilterSelector(
          title: Text("Biên tập hình ảnh"),
          image: image,
          filters: a.presetCustomerFiltersList,
          filename: fileName,
          loader: Center(child: CircularProgressIndicator()),
          fit: BoxFit.contain,
        ),
      ),
    );
    if (imagefile != null && imagefile.containsKey('image_filtered')) {
      setState(() {
        imageFile = imagefile['image_filtered'];
      });
      print(imageFile.path);
    }
  }

  PainterController _newController() {
    PainterController controller = new PainterController();
    controller.thickness = 5.0;
    controller.backgroundColor = Colors.green;
    return controller;
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> actions;

      actions = <Widget>[
        new IconButton(
          icon: new Icon(Icons.content_copy),
          tooltip: 'New Painting',
          onPressed: () => setState(() {
            _finished = false;
            _controller = _newController();
          }),
        ),
      ];


    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Biên tập hình ảnh'),
        actions: actions
      ),
      body: Center(
        child: new Container(
          child: imageFile == null
              ? Center(
                  child: new Text('No image selected.'),
                )
              : Image.file(imageFile),
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: () => getImage(context),
        tooltip: 'Pick Image',
        child: new Icon(Icons.add_a_photo),
      ),
    );
  }
}

// class Color {
//   String name;
//   var value;
//
//   Color(this.name, this.value);
//
//   Color.fromJson(Map<String, dynamic> json)
//       : name = json['name'],

//
//   Map<String, dynamic> toJson() =>
//       {
//         'name': name,
//         'value': value,
//       };
// }

